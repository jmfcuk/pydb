import pyodbc
import pandas as pd

def main():
    cnxn = pyodbc.connect("Driver={SQL Server Native Client 11.0};"
                      "Server=john-windows;"
                      "Database=Items;"
                      "Trusted_Connection=yes;")

    # df = pd.read_sql_query('select * from Item', cnxn)

    # print(df.head())

    cursor = cnxn.cursor()
    #cursor.execute('SELECT * FROM Item i JOIN Data d ON d.ItemId = i.Id')
    cursor.tables()
    rows = cursor.fetchall()
    for row in rows:
        print('row = {}'.format(row.table_name,))

if __name__ == '__main__':
    main()
